# About

Misc scripts involving rofi:

- `rofi-ykman-oath`: Copy yubikey oath tokens into the clipboard
- `rofi-nmcli`: Toggle network states

# Install

Put the scripts somewhere in your `$PATH`.
With stow, from within this repository:

```sh
stow -c "${HOME}/.local/bin" .
```
